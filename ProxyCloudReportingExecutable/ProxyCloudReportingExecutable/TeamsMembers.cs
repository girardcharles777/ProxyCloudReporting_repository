﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ProxyCloudReportingExecutable.RequestAPI;

namespace ProxyCloudReportingExecutable
{
    internal class TeamsMembers
    {
        public string teamID;
        public List<TeamMembers> ListMembersOfTeam;
        public string teamNom;
        public TeamsMembers() { }

        public TeamsMembers(string idTeams, List<TeamMembers> listMembersOfTeam, string teamNom)
        {
            this.teamID = idTeams;
            ListMembersOfTeam = listMembersOfTeam;
            this.teamNom = teamNom;
        }
    }
}
