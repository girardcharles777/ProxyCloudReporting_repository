﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ProxyCloudReportingExecutable.RequestAPI;

namespace ProxyCloudReportingExecutable
{
    internal class AgentSession
    {
        public string agentID;
        public List<ResultMetric> listRsultMetric;

        public AgentSession(string _agentId, List<ResultMetric> _listRsultMetric) {
            this.agentID = _agentId;
            this.listRsultMetric = _listRsultMetric;
        }
    }
}
