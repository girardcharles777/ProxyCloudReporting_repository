﻿using Newtonsoft.Json;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static ProxyCloudReportingExecutable.RequestAPI;

namespace ProxyCloudReportingExecutable
{
    internal class RequestAPI
    {

        public static List<User> GET_Users(string api, string token)
        {
            bool completed = false;

            string url = api + "/api/v2/users";
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Headers.Add("Content-Type", "application/json");

            List<User> list_us = new List<User>();

            while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                UserClass us = JsonConvert.DeserializeObject<UserClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }
            return list_us;
        }

        public static List<Conversation> Get_Conversation(string api, string token, string interval, string order, string orderBy)
        {

            bool completed = false;

            int pageNumber = 1;
            List<Conversation> list_conversation = new List<Conversation>();
            string url = api + "/api/v2/analytics/conversations/details/query";

            while (!completed)
            {

                WebClient client = new WebClient();
                client.Headers.Add("cache-control", "no-cache");
                client.Headers.Add("Content-Type", "application/json");
                client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;


                string body = "{" +
                            "\"interval\": \"" + interval + "\"" +
                            ",\"order\": \"" + order + "\"" +
                            ",\"orderBy\": \"" + orderBy + "\"" +
                            ",\"paging\": {" +
                                "\"pageSize\": 100," +
                                "\"pageNumber\":" + pageNumber +
                            "}" +
                            "}";


                byte[] data = client.UploadData(url, Encoding.UTF8.GetBytes(body));
                ConversationClass conv = JsonConvert.DeserializeObject<ConversationClass>(Encoding.UTF8.GetString(data));

                //Boucle qui verifie si il reste encore des conversations et sinon met stop la fonction while
                if (conv.Conversations != null)
                {
                    list_conversation.AddRange(conv.Conversations);
                    pageNumber += 1;
                }
                else
                {
                    completed = true;
                }

            }

            return list_conversation;
        }

        public static List<ParticipantAttributes> GET_attribut_conversation(string api, string token, string idConversation)
        {

            string url = api + "/api/v2/conversations/calls/" + idConversation;
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Encoding = Encoding.UTF8;

            List<ParticipantAttributes> list_us = new List<ParticipantAttributes>();

            //On récupère la réponse en format JSON
            string data = client.DownloadString(url);
            AttributesClass us = JsonConvert.DeserializeObject<AttributesClass>(data);

            list_us.AddRange(us.Participant);

            if (client.QueryString == null)
                client.QueryString = new System.Collections.Specialized.NameValueCollection();
            else
                client.QueryString.Clear();

            return list_us;
        }


        public static List<ResultMetric> POST_MetricsUser(string api,string interval, string token, string userid)
        {

            string url = api + "/api/v2/analytics/users/aggregates/query";

            WebClient client = new WebClient();
            client.Headers.Add("cache-control", "no-cache");
            client.Headers.Add("Content-Type", "application/json");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            string body = "{" +
                            "\"interval\": \"" + interval + "\"," +
                            "\"groupBy\": []," +
                            "\"filter\": {" +
                               "\"type\": \"or\"," +
                               "\"predicates\": [ " +
                                "{" +
                                    "\"type\": \"dimension\"," +
                                    "\"dimension\": \"userId\"," +
                                    "\"operator\": \"matches\"," +
                                    "\"value\": \"" + userid + "\"" +
                                "}" +
                            "]" +
                            "}," +
                            "\"metrics\": [ " +                               
                                "\"tOrganizationPresence\"" +
                            "]" +
                           "}";
            byte[] data = client.UploadData(url, Encoding.UTF8.GetBytes(body));
            string dataS = Encoding.UTF8.GetString(data);

            UserQueryClass uqc = JsonConvert.DeserializeObject<UserQueryClass>(dataS);
            if (uqc.Results == null || uqc.Results.Count == 0) return new List<ResultMetric>();

            return uqc.Results.First<Result>().Data.First<ResultData>().Metrics;
        }

        public static List<PresenceDefinition> GET_presenceDefinition(string api, string token)
        {

            bool completed = false;

            string url = api + "/api/v2/presencedefinitions";
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Headers.Add("Content-Type", "application/json");

            List<PresenceDefinition> list_us = new List<PresenceDefinition>();

            while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                PresenceDefinitionsClass us = JsonConvert.DeserializeObject<PresenceDefinitionsClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }
            return list_us;
        }

        //Pour les works teams 
        public static List<Team> GET_workTeams(string api, string token)
        {
            bool completed = false;

            string url = api + "/api/v2/teams";
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Headers.Add("Content-Type", "application/json");

            client.Encoding = Encoding.UTF8;

            List<Team> list_us = new List<Team>();

            while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                TeamsClass us = JsonConvert.DeserializeObject<TeamsClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }
            return list_us;
        }

        //Pour les works teams 
        public static List<TeamMembers> GET_membersOfTeam(string api, string token, string teamID )
        {
            bool completed = false;

            string url = api + "/api/v2/teams/" + teamID + "/members";
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Headers.Add("Content-Type", "application/json");

            List<TeamMembers> list_us = new List<TeamMembers>();

            while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                TeamsMembersClass us = JsonConvert.DeserializeObject<TeamsMembersClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }
            return list_us;
        }




        //----------------------------------------Les classes------------------------------------------
        public class User
        {
            [JsonProperty(PropertyName = "id")]
            public string UserID { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "state")]
            public string State { get; set; }
            [JsonProperty(PropertyName = "email")]
            public string Email { get; set; }
            public User(string _id, string _name, string _email, string _state)
            {
                UserID = _id;
                Name = _name;
                Email = _email;
                State = _state;
            }
            public User()
            {
            }
        }

        public class UserClass
        {
            [JsonProperty(PropertyName = "entities")]
            public List<User> Entities { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public UserClass(List<User> _ent, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Entities = _ent;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public UserClass()
            {
            }
        }

        class ConversationClass
        {
            [JsonProperty(PropertyName = "conversations")]
            public List<Conversation> Conversations { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public ConversationClass(List<Conversation> _conv, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Conversations = _conv;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public ConversationClass()
            {
            }
        }

        public class Conversation
        {
            [JsonProperty(PropertyName = "conversationId")]
            public string ConversationID { get; set; }
            [JsonProperty(PropertyName = "conversationStart")]
            public DateTime ConversationStart { get; set; }
            [JsonProperty(PropertyName = "conversationEnd")]
            public DateTime ConversationEnd { get; set; }
            [JsonProperty(PropertyName = "originatingDirection")]
            public string OriginatingDirection { get; set; }
            [JsonProperty(PropertyName = "participants")]
            public List<Participant> participants { get; set; }

            public Conversation(string _id, DateTime _start, DateTime _end, string _direction, List<Participant> _participantId)
            {
                ConversationID = _id;
                ConversationStart = _start;
                ConversationEnd = _end;
                OriginatingDirection = _direction;
                participants = _participantId;
            }
            public Conversation()
            {
            }
        }



        public class Participant
        {
            [JsonProperty(PropertyName = "participantId")]
            public string ParticipantID { get; set; }
            [JsonProperty(PropertyName = "participantName")]
            public string ParticipantName { get; set; }
            [JsonProperty(PropertyName = "purpose")]
            public string Purpose { get; set; }
            [JsonProperty(PropertyName = "userId")]
            public string UserId { get; set; }
            [JsonProperty(PropertyName = "sessions")]
            public List<Session> Session { get; set; }

            public Participant(string _participantId, string _participantName, string _purp, string _userId, List<Session> _session)
            {
                ParticipantID = _participantId;
                ParticipantName = _participantName;
                Purpose = _purp;
                UserId = _userId;
                Session = _session;
            }
            public Participant()
            {
            }
        }


        public class Session
        {
            [JsonProperty(PropertyName = "ani")]
            public string Ani { get; set; }
            [JsonProperty(PropertyName = "dnis")]
            public string Dnis { get; set; }
            [JsonProperty(PropertyName = "addressFrom")]
            public string AddressFrom { get; set; }
            [JsonProperty(PropertyName = "addressTo")]
            public string AddressTo { get; set; }
            [JsonProperty(PropertyName = "mediaType")]
            public string MediaType { get; set; }
            [JsonProperty(PropertyName = "sessionId")]
            public string SessionId { get; set; }
            [JsonProperty(PropertyName = "selectedAgentId")]
            public string SelectedAgentId { get; set; }
            [JsonProperty(PropertyName = "metrics")]
            public List<Metrics> Metrics { get; set; }
            [JsonProperty(PropertyName = "segments")]
            public List<Segment> Segments { get; set; }
            [JsonProperty(PropertyName = "flow")]
            public Flow Flow { get; set; }


            public Session(string _ani, string _dnis, string _mediaType, string _sessionId, string _selectedAgentId, List<Metrics> _metrics, List<Segment> _segment, Flow _flow)
            {
                Ani = _ani;
                Dnis = _dnis;
                MediaType = _mediaType;
                SessionId = _sessionId;
                SelectedAgentId = _selectedAgentId;
                Metrics = _metrics;
                Segments = _segment;
                Flow = _flow;

            }
            public Session()
            {
            }
        }

        public class Flow
        {
            [JsonProperty(PropertyName = "outcomes")]
            public List<Outcomes> Outcomes { get; set; }

            [JsonProperty(PropertyName = "flowId")]
            public string FlowId { get; set; }

            [JsonProperty(PropertyName = "flowName")]
            public string FlowName { get; set; }


            public Flow(List<Outcomes> _outcomes, string _flowID, string _flowName)
            {
                Outcomes = _outcomes;
                FlowId = _flowID;
                FlowName = _flowName;


            }
            public Flow()
            {
            }
        }


        public class Outcomes
        {
            [JsonProperty(PropertyName = "flowOutcome")]
            public string FlowOutcome { get; set; }
            [JsonProperty(PropertyName = "flowOutcomeEndTimestamp")]
            public string FlowOutcomeEndTimestamp { get; set; }
            [JsonProperty(PropertyName = "flowOutcomeId")]
            public string FlowOutcomeId { get; set; }
            [JsonProperty(PropertyName = "flowOutcomeStartTimestamp")]
            public string FlowOutcomeStartTimestamp { get; set; }
            [JsonProperty(PropertyName = "flowOutcomeValue")]
            public string FlowOutcomeValue { get; set; }

            public Outcomes(string _flowOutcome, string _flowOutcomeEndTimestamp, string _flowOutcomeId, string _flowOutcomeStartTimestamp, string _flowOutcomeValue)
            {
                FlowOutcome = _flowOutcome;
                FlowOutcomeEndTimestamp = _flowOutcomeEndTimestamp;
                FlowOutcomeId = _flowOutcomeId;
                FlowOutcomeStartTimestamp = _flowOutcomeStartTimestamp;
                FlowOutcomeValue = _flowOutcomeValue;
            }
            public Outcomes()
            {
            }
        }

        public class Metrics
        {
            [JsonProperty(PropertyName = "emitDate")]
            public DateTime EmitDate { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "value")]
            public int Value { get; set; }

            public Metrics(DateTime _emitDate, string _Name, int _Value)
            {
                EmitDate = _emitDate;
                Name = _Name;
                Value = _Value;

            }
            public Metrics()
            {
            }
        }


        public class Segment
        {
            [JsonProperty(PropertyName = "queueId")]
            public string QueueId { get; set; }
            [JsonProperty(PropertyName = "segmentEnd")]
            public DateTime SegmentEnd { get; set; }
            [JsonProperty(PropertyName = "segmentStart")]
            public DateTime segmentStart { get; set; }
            [JsonProperty(PropertyName = "segmentType")]
            public string SegmentType { get; set; }
            [JsonProperty(PropertyName = "wrapUpCode")]
            public string WrapUpCode { get; set; }

            public Segment(string _queueId, DateTime _segmentEnd, DateTime _segmentStart, string _segmentType, string _wrapUpCode)
            {
                QueueId = _queueId;
                SegmentEnd = _segmentEnd;
                segmentStart = _segmentStart;
                SegmentType = _segmentType;
                WrapUpCode = _wrapUpCode;

            }
            public Segment()
            {
            }
        }


        class AttributesClass
        {
            

            [JsonProperty(PropertyName = "participants")]
            public List<ParticipantAttributes> Participant { get; set; }

            public AttributesClass( List<ParticipantAttributes> _participant)
            {
               
                Participant = _participant;

            }
            public AttributesClass()
            {
            }
        }

        public class ParticipantAttributes
        {
            [JsonProperty(PropertyName = "id")]
            public string ParticipantID { get; set; }
            [JsonProperty(PropertyName = "attributes")]
            public Dictionary<string, string> DictionaryAttributes { get; set; }
            public ParticipantAttributes(string _id, Dictionary<string, string> _dictionaryAttributes)
            {
                ParticipantID = _id;
                DictionaryAttributes = _dictionaryAttributes;
            }
            public ParticipantAttributes()
            {
            }
        }


        //Pour les metrics agent (busy, work...)
        public class UserQueryClass
        {
            [JsonProperty(PropertyName = "results")]
            public List<Result> Results { get; set; }
            public UserQueryClass(List<Result> _ent)
            {
                Results = _ent;
            }
            public UserQueryClass()
            {
            }
        }
        public class Result
        {
            [JsonProperty(PropertyName = "data")]
            public List<ResultData> Data { get; set; }
            public Result(List<ResultData> _data)
            {
                Data = _data;
            }
            public Result()
            {
            }
        }

        public class ResultData
        {
            [JsonProperty(PropertyName = "metrics")]
            public List<ResultMetric> Metrics { get; set; }

            public ResultData(List<ResultMetric> _met)
            {
                Metrics = _met;
            }
            public ResultData()
            {
            }
        }

        public class ResultMetric
        {
            [JsonProperty(PropertyName = "metric")]
            public string Metric { get; set; }
            [JsonProperty(PropertyName = "qualifier")]
            public string Qualifier { get; set; }

            [JsonProperty(PropertyName = "stats")]
            public Stats Stats { get; set; }
            public ResultMetric(string _met, String _qual, Stats _stat)
            {
                Metric = _met;
                Qualifier = _qual;
                Stats = _stat;
            }
            public ResultMetric()
            {
            }
        }

        public class Stats
        {
            [JsonProperty(PropertyName = "sum")]
            public int Sum { get; set; }
        }


        public class PresenceDefinitionsClass
        {
            [JsonProperty(PropertyName = "entities")]
            public List<PresenceDefinition> Entities { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public PresenceDefinitionsClass(List<PresenceDefinition> _ent, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Entities = _ent;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public PresenceDefinitionsClass()
            {
            }
        }

        public class PresenceDefinition
        {
            [JsonProperty(PropertyName = "id")]
            public string PresenceDefinitionID { get; set; }
            [JsonProperty(PropertyName = "systemPresence")]
            public string systemPresenceName { get; set; }

            [JsonProperty(PropertyName = "languageLabels")]
            public Language languageLabels { get; set; }
            public PresenceDefinition(string _id, string _systemPresence, Language languageLabels)
            {
                PresenceDefinitionID = _id;
                systemPresenceName = _systemPresence;
                this.languageLabels = languageLabels;
            }
            public PresenceDefinition()
            {
            }
        }

        public class Language
        {
            [JsonProperty(PropertyName = "en_US")]
            public string en_US { get; set; }
        }


        //Pour les Work teams : 
        public class Team
        {
            [JsonProperty(PropertyName = "id")]
            public string teamID { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string teamName { get; set; }
            
            public Team(string _id, string _name)
            {
                teamID = _id;
                teamName = _name;
        
            }
            public Team()
            {
            }
        }

        public class TeamsClass
        {
            [JsonProperty(PropertyName = "entities")]
            public List<Team> Entities { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public TeamsClass(List<Team> _ent, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Entities = _ent;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public TeamsClass()
            {
            }
        }


        public class TeamMembers
        {
            [JsonProperty(PropertyName = "id")]
            public string agentID { get; set; }         

            public TeamMembers(string _id)
            {
                agentID = _id;

            }
            public TeamMembers()
            {
            }
        }

        public class TeamsMembersClass
        {
            [JsonProperty(PropertyName = "entities")]
            public List<TeamMembers> Entities { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public TeamsMembersClass(List<TeamMembers> _ent, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Entities = _ent;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public TeamsMembersClass()
            {
            }
        }
    }
}
