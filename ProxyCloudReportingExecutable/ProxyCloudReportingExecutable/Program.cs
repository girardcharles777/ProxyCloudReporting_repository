﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions;
using PureCloudPlatform.Client.V2.Model;
using Renci.SshNet;
using static System.Net.Mime.MediaTypeNames;
using static ProxyCloudReportingExecutable.RequestAPI;

namespace ProxyCloudReportingExecutable
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-------------------------------------------------Start-------------------------------------------------");

            //Déclarez un objet de type StreamWriter pour écrire les lignes de log dans un fichier. 
            StreamWriter logFile = new StreamWriter("C:\\Users\\Charles\\Documents\\Client Cegedim\\logs\\fichier.log", true);

            logFile.WriteLine();
            logFile.WriteLine("----------------------------------------------" + $"{DateTime.Now}" + "----------------------------------------------");

            //--------------------------------------RECUPERAION DES PARAMETRES DE CONFIGURATION---------------------------------------------------------------------------

            logFile.Write("Recupération des pametres de configuration (filtre Participant data, siteetintervalle de date, statut : ");

            //Filtre site
            List<string> siteFilter = new List<string>();
            List<string> participantDataFilter = new List<string>();

            try
            {

                string citiesValue = ConfigurationManager.AppSettings["Site"];
                if (!string.IsNullOrEmpty(citiesValue))
                {
                    string[] cityArray = citiesValue.Split(',');
                    siteFilter.AddRange(cityArray);
                }

                //Filtre Participant Data


                string participantDataValue = ConfigurationManager.AppSettings["ParticipantData"];
                if (!string.IsNullOrEmpty(participantDataValue))
                {
                    string[] participantArray = participantDataValue.Split(',');
                    participantDataFilter.AddRange(participantArray);
                }

                logFile.WriteLine("OK");

            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);
            }


            //--------------------------------------INTERVALLE DE DATE------------------------------------------------------------------------------
            /*Récupération des dates et heure : maintenant UTC - 24h et maintenant */

            logFile.WriteLine("Création de l'intervalle des dates : ");


            string dateDebutValue = ConfigurationManager.AppSettings["DateDebut"];
            string dateFinValue = ConfigurationManager.AppSettings["DateFin"];

            DateTime dateDebut;
            DateTime dateFin;
            TimeSpan intervalle;

            string debutIntervalleString = null;
            string finIntervalleString = null;

            string interval;

            //Si pas vide, intervalle à la main, sinon automatique (hier + aujourdhui)
            if(!string.IsNullOrEmpty(dateDebutValue) && !string.IsNullOrEmpty(dateFinValue))
            {
                logFile.Write("Intervalle saisie à la main, statut : ");

                if (!DateTime.TryParse(dateDebutValue, out dateDebut))
                {
                    logFile.WriteLine("KO : La valeur de DateDebut n'est pas dans un format de date valide.");
                    logFile.Close();
                    Environment.Exit(0);
                }
                else if (!DateTime.TryParse(dateFinValue, out dateFin))
                {
                    logFile.WriteLine("KO : La valeur de DateFin n'est pas dans un format de date valide.");
                    logFile.Close();
                    Environment.Exit(0);
                }
                else if (dateDebut >= dateFin)
                {
                    logFile.WriteLine("KO : La date de début doit être antérieure à la date de fin.");
                    logFile.Close();
                    Environment.Exit(0);
                }
                else
                {
                    intervalle = dateFin - dateDebut;
                    if (intervalle.TotalHours > 24)
                    {
                        logFile.WriteLine("KO : L'intervalle entre la date de début et la date de fin dépasse 24 heures.");
                        logFile.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Les dates sont valides et respectent les contraintes.");
                        debutIntervalleString = dateDebut.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                        finIntervalleString = dateFin.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

                    }
                }
            }
            else
            {

                logFile.Write("Intervalle automatique, statut : ");

                DateTime dateNow = DateTime.Now;
                DateTime dateNowUTC = dateNow.AddHours(-2);
                DateTime yesterdayUTC = dateNowUTC.AddDays(-1);

                finIntervalleString = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                debutIntervalleString = yesterdayUTC.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

                //string dateNowUTCString = "2023-06-09T01:00:00.000Z";
                //string yesterdayUTCString = "2023-06-08T01:00:00.000Z";

            }

            interval = debutIntervalleString + "/" + finIntervalleString;

            logFile.WriteLine("OK : " + interval);



            //--------------------------------------CONNEXION GENESYS CLOUD------------------------------------------------------------------------------
            /*Connexion au cloud Genesys de l'entreprise
             *Récupération d'un token de connexion pour l'utilisation par la suite des requettes API*/

            string clientID = ConfigurationManager.AppSettings["ClientID"];
            string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            string API_URL = ConfigurationManager.AppSettings["API_URL"];
            string token = "";

            try
            {
                //Define the region
                PureCloudRegionHosts region = PureCloudRegionHosts.eu_central_1; // Genesys Cloud region
                PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.setBasePath(region);

                // Configure OAuth2 access token for authorization: PureCloud OAuth
                // The following example is using the Client Credentials Grant
                var accessTokenInfo = PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.PostToken(clientID, clientSecret);
                token = accessTokenInfo.AccessToken;
            }
            catch (Exception ex)
            {
                logFile.WriteLine(" ERREUR - Récupération du TOKEN impossible : " + ex.Message);
            }


            //--------------------------------------RECUPERATION DES USERS------------------------------------------------------------------------------


            logFile.Write("Récupération des users, statut : ");
            List<RequestAPI.User> list_users = new List<RequestAPI.User>();

            try
            {
                //Récupération des users :
                list_users = RequestAPI.GET_Users(API_URL, token);
                logFile.WriteLine("OK");
            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);
            }

            //--------------------------------------RECUPERATION DES CONVERSATIONS, SESSIONS, PARTICIPANTS, SEGMENTS, METRICS------------------------------------------------------------------------------
            
            logFile.Write("Récupération des conversations, sessions, participants, segments, metrics, statut : ");
            
            //on commance par récupérer toutes les informations du JSON sans triage
            List<RequestAPI.Conversation> list_conversation = new List<RequestAPI.Conversation>();

            try
            {
                               
                list_conversation = RequestAPI.Get_Conversation(API_URL, token, interval, "asc", "conversationStart");
                logFile.WriteLine("OK");
                
            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);
            }


            //--------------------------------------RECUPERATION DES PARTICIPANTS DATA------------------------------------------------------------------------------

            logFile.Write("Récupération des participants data, statut : ");

            List<ParticipantDataTable> listParticipantsData = new List<ParticipantDataTable>();
            try
            {
                foreach (RequestAPI.Conversation conversation in list_conversation)
                {
                    List<RequestAPI.ParticipantAttributes> list_attribut_participant = RequestAPI.GET_attribut_conversation(API_URL, token, conversation.ConversationID);
                    ParticipantDataTable ParticipantData = new ParticipantDataTable(conversation.ConversationID, list_attribut_participant);
                    listParticipantsData.Add(ParticipantData);
                }

                logFile.WriteLine("OK");

            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);

            }

            //--------------------------------------RECUPERATION DE LA LISTE DES PRESENCE DEFINITION------------------------------------------------------------------------------
            List<RequestAPI.PresenceDefinition> list_presenceDefinition= RequestAPI.GET_presenceDefinition(API_URL, token);          

            //--------------------------------------RECUPERATION DES SESSION AGENT------------------------------------------------------------------------------

            logFile.Write("Récupération des sessions agents, statut : ");

            List<ResultMetric> listresultMetrics = new List<ResultMetric>();
            List<AgentSession> listeAgentSession= new List<AgentSession>();

            try
            {
                foreach(RequestAPI.User user in list_users)
                {
                    listresultMetrics = RequestAPI.POST_MetricsUser(API_URL, interval, token, user.UserID);                  
                    AgentSession agentSession = new AgentSession(user.UserID, listresultMetrics);
                    listeAgentSession.Add(agentSession);
                }               

                logFile.WriteLine("OK");

            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);

            }

            //--------------------------------------RECUPERATION DES TEAMS------------------------------------------------------------------------------
            
            logFile.Write("Récupération des works teams, statut : ");

            List<RequestAPI.Team> list_worksTeams = new List<RequestAPI.Team> ();


            try
            {
                list_worksTeams = GET_workTeams(API_URL, token);
                logFile.WriteLine("OK");

            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);
            }

            //--------------------------------------RECUPERATION DES USERS TEAMS------------------------------------------------------------------------------

            logFile.Write("Récupération des works teams pour chaque utilisateur, statut : ");

            List<RequestAPI.TeamMembers> list_membersOfTeam = new List<RequestAPI.TeamMembers>();
            List<TeamsMembers> list_teamsMembers = new List<TeamsMembers>();

            try
            {
                for (int i = 0; i < list_worksTeams.Count; i++)
                {
                    list_membersOfTeam = GET_membersOfTeam(API_URL, token, list_worksTeams[i].teamID);
                    TeamsMembers teamMembers = new TeamsMembers(list_worksTeams[i].teamID, list_membersOfTeam, list_worksTeams[i].teamName);
                    list_teamsMembers.Add(teamMembers);
                }
                logFile.WriteLine("OK");

            }
            catch (Exception ex)
            {
                logFile.WriteLine("KO : " + ex.Message);
            }


            //--------------------------------------CONNEXION SFTP------------------------------------------------------------------------------

            string host = ConfigurationManager.AppSettings["host"];
            int port = int.Parse(ConfigurationManager.AppSettings["port"]);
            string username = ConfigurationManager.AppSettings["username"];
            string encodedPassword = ConfigurationManager.AppSettings["password"];
            byte[] passwordBytes = Convert.FromBase64String(encodedPassword);
            string password = Encoding.UTF8.GetString(passwordBytes);
            string remoteFilePath = ConfigurationManager.AppSettings["remoteFilePath"];
            

            logFile.Write("Connexion serveur FTP / SFTP, statut : ");


            // Créer une connexion SFTP
            using (var client = new SftpClient(host, port, username, password))
            {
                // Se connecter au serveur SFTP
                client.Connect();

                if (client.IsConnected)
                {
                    logFile.WriteLine("OK");

                    //Date pour le nommage de l'excel :
                    DateTime yesterdayDateTime = DateTime.Parse(debutIntervalleString);
                    string dateExcel = yesterdayDateTime.ToString("yyyy-MM-dd");

                    //--------------------------------------EXPORTATION DES CONVERSATIONS -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des conversations -> CSV, statut : ");

                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathConversation = remoteFilePath + "Cegedim-IFACTS-" + dateExcel + ".csv";
                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("CONVERSATION_ID;ANI;DNIS;INTERACTION_TYPE;MEDIA;INTERACTION_DEB;INTERACTION_FIN;STATUT_INTERACTION");

                                // Écrire les données de chaque conversation dans le fichier CSV
                                foreach (RequestAPI.Conversation conversation in list_conversation)
                                {
                                    string ani = conversation.participants[0].Session[0].Ani;
                                    string dnis = conversation.participants[0].Session[0].Dnis;
                                    if (ani.StartsWith("tel:+33"))
                                    {
                                        ani = '0' + ani.Substring(7);
                                    }
                                    else
                                    {
                                        ani = null;
                                    }
                                    if (dnis.StartsWith("tel:+33"))
                                    {
                                        dnis = '0' + dnis.Substring(7);
                                    }


                                    writer.WriteLine($"{conversation.ConversationID};{ani};{dnis};{conversation.OriginatingDirection};{conversation.participants[0].Session[0].MediaType};{conversation.ConversationStart};{conversation.ConversationEnd};null");
                                }

                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathConversation);

                            }
                            logFile.WriteLine("OK");
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }
                    }
                    

                    //--------------------------------------EXPORTATION DES RESSOURCES (segments) -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des Ressources -> CSV, statut : ");

                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathRessourceSegment = remoteFilePath + "Cegedim-IRFACTS-" + dateExcel + ".csv";

                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("CONVERSATION_ID;SEGMENT_ID;SEGMENT_TYPE;RESOURCE_TYPE;PARTICIPANT_NAME;AGENT/USER_ID;SEGMENT_DEB;SEGMENT_FIN;STATUT;CONVERSATION_DUREE;EN_GARDE_DUREE;SONNERIE_DUREE;ACW_DUREE;CONSULTATION_DUREE");

                                //On commence par parcourir les conversations
                                foreach (RequestAPI.Conversation conversation in list_conversation)
                                {
                                    int segmentID = 0;
                                    //Pour chaque conversation, il y a un liste de participants qu'il faut aussi parcourir
                                    foreach (RequestAPI.Participant participant in conversation.participants)
                                    {
                                        //Pour chaque participant, il y a qu'une seul session donc session[0]
                                        //Mais pour une session, il y a une liste de metrics et de segments et c'est chaque segment que l'on va ajouter dans les lignes du csv :
                                        //En ajoutant quand c'est nécessaires, les metrics demandées ( tTalk, tHeldComplete...) 
                                        //On va donc vérifier pour chaque session, si il y a les metrics intéressantes

                                        Dictionary<string, int> dictionaryMetrics = new Dictionary<string, int>()
                                        {
                                             {"tTalk", 0},
                                             {"tHeldComplete", 0},
                                             {"tAlert", 0},
                                             {"tAcw", 0}
                                        };

                                        //Metrics
                                        foreach (RequestAPI.Metrics metrics in participant.Session[0].Metrics ?? Enumerable.Empty<RequestAPI.Metrics>())
                                        {
                                            if (dictionaryMetrics.ContainsKey(metrics.Name))
                                            {
                                                dictionaryMetrics[metrics.Name] = metrics.Value;
                                            }
                                        }

                                        //Parfois il y a des doubons ( deux interact différents mais la metrics prend la somme des deux donc il faut l'afficher que sur une seul.
                                        bool interactPresent = false;
                                        //segments                            
                                        foreach (RequestAPI.Segment segment in participant.Session[0].Segments)
                                        {
                                            if (participant.Purpose == "botflow")
                                            {
                                                participant.ParticipantName = participant.Session[0].Flow.FlowName;
                                            }

                                            if (participant.Purpose != "customer" && segment.SegmentType != "system" && segment.SegmentType != "customer")//Comme les lignes customer représente l'appel en entier et non les segments, on les retires:
                                            {


                                                segmentID += 1;
                                                int enGarde = 0;
                                                int interact = 0;
                                                int wrapup = 0;
                                                int alert = 0;

                                                //Pour eviter d'ajouter les 4 données pour chaque ligne 'dialing,contactiong,interact,wrapup' on fait un switch case : 
                                                switch (segment.SegmentType)
                                                {
                                                    case "hold"://
                                                        enGarde = dictionaryMetrics["tHeldComplete"] / 1000;
                                                        break;

                                                    case "interact"://
                                                        if (!interactPresent)
                                                        {
                                                            interact = dictionaryMetrics["tTalk"] / 1000;
                                                            interactPresent = true;
                                                        }
                                                        break;

                                                    case "wrapup"://
                                                        wrapup = dictionaryMetrics["tAcw"] / 1000;
                                                        break;

                                                    case "alert"://
                                                        alert = dictionaryMetrics["tAlert"] / 1000;

                                                        break;
                                                    default:
                                                        break;
                                                }
                                                writer.WriteLine($"{conversation.ConversationID};{segmentID};{segment.SegmentType};{participant.Purpose};{participant.ParticipantName};{participant.UserId};{segment.segmentStart};{segment.SegmentEnd};null;{interact};{enGarde};{alert};{wrapup};0");
                                            }
                                        }

                                    }

                                }

                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathRessourceSegment);

                                logFile.WriteLine("OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }
                    }
                    

                    //--------------------------------------EXPORTATION DES PARTICIPANT DATA -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des participants data -> CSV, statut : ");
                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathParticipantsData = remoteFilePath + "Cegedim-PADATA-" + dateExcel + ".csv";

                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("CONVERSATION_ID;PARTICIPANT_DATA;PARTICIPANT_DATA_VALUE");

                                // Écrire les données de chaque conversation dans le fichier CSV
                                //On commence par parcourir la liste de toutes les conversationID, composées de tout les participantIS, composé d'une iste d'attribut
                                //Attention : la liste d'attribut pour un participant est souvent nul. 
                                foreach (ParticipantDataTable participantData in listParticipantsData)
                                {
                                    foreach (RequestAPI.ParticipantAttributes participantAttributes in participantData.listparticipantAttributes)
                                    {
                                        //Vérifie si la liste des attributs n'est pas nul:
                                        if (participantAttributes.DictionaryAttributes.Count != 0)
                                        {
                                            //On parcour la liste des <key;value>
                                            foreach (KeyValuePair<string, string> attributes in participantAttributes.DictionaryAttributes)
                                            {
                                                //On ajouter le filtre edmandé par Cegedim : 
                                                if (participantDataFilter.Contains(attributes.Key) || participantDataFilter.Count == 0)
                                                {
                                                    writer.WriteLine($"{participantData.conversationID};{attributes.Key};{attributes.Value}");
                                                }
                                            }
                                        }
                                    }
                                }

                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathParticipantsData);

                                logFile.WriteLine("OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }

                    }

                    

                    //--------------------------------------EXPORTATION DES AGENTS -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des Agents data -> CSV, statut : ");

                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathAgent = remoteFilePath + "Cegedim-AGENTS-" + dateExcel + ".csv";

                        //Pour l'instant, date bidon  :
                        DateTime dateActivateDebut = new DateTime(1990, 01, 01);

                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("AGENTID;AGENT_NAME;STATE;ACTIVATE_DEB;ACTIVATE_FIN");

                                // Écrire les données de chaque conversation dans le fichier CSV
                                //On commence par parcourir la liste de toutes les conversationID, composées de tout les participantIS, composé d'une iste d'attribut
                                //Attention : la liste d'attribut pour un participant est souvent nul. 
                                foreach (RequestAPI.User user in list_users)
                                {
                                    writer.WriteLine($"{user.UserID};{user.Name};{user.State};{dateActivateDebut};{null}");

                                }

                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathAgent);

                                logFile.WriteLine("OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }

                    }


                    //--------------------------------------EXPORTATION DES SESSION AGENT -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des sessions agent -> CSV, statut : ");

                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathSession = remoteFilePath + "Cegedim-SESSIONS_AGENTS-" + dateExcel + ".csv";

                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("AGENTID;JOUR;TPS_EN_FILE_ATTENTE;TPS_HORS_FILE_ATTENTE;TPS_PRET_COLL;TPS_DEJEUNER;TPS_COACHING;TPS_ADMIN;TPS_BACKOFFICE;TPS_FIN_APPELS_ENTRANTS;TPS_FIN_APPELS_SORTANTS;TPS_DETENTE;TPS_IT");

                                //On va dans un premier temps parcourir la liste des agents avec leurs sessions :
                                foreach (AgentSession agentSession in listeAgentSession)
                                {

                                    Dictionary<string, int> dictionaryResult = new Dictionary<string, int>()
                                    {
                                         {"Admin", 0},
                                         {"Back Office", 0},
                                         {"Busy", 0},
                                         {"IT", 0},
                                         {"DÃ©tente", 0},
                                         {"Available", 0},
                                         {"Fin d'appel entrants", 0},
                                         {"Fin d'appel sortants", 0},
                                         {"Coaching", 0},
                                         {"Offline", 0},
                                         {"On Queue", 0}
                                    };


                                    //On va parcourir la liste des sessions de l'agent :
                                    foreach (ResultMetric session in agentSession.listRsultMetric)
                                    {
                                        foreach (RequestAPI.PresenceDefinition presence in list_presenceDefinition)
                                        {

                                            if (presence.PresenceDefinitionID == session.Qualifier)
                                            {
                                                dictionaryResult[presence.languageLabels.en_US] = session.Stats.Sum / 1000;
                                            }
                                        }
                                    }
                                    writer.WriteLine($"{agentSession.agentID};{dateExcel};{dictionaryResult["On Queue"]};{dictionaryResult["Offline"]};{dictionaryResult["Available"]};{dictionaryResult["Admin"]};{dictionaryResult["Coaching"]};{dictionaryResult["Busy"]};{dictionaryResult["Back Office"]};{dictionaryResult["Fin d'appel entrants"]};{dictionaryResult["Fin d'appel sortants"]};{dictionaryResult["DÃ©tente"]};{dictionaryResult["IT"]} ");

                                }
                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathSession);

                                logFile.WriteLine("OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }
                    }

                    //--------------------------------------EXPORTATION DES SITES -> CSV------------------------------------------------------------------------------

                    logFile.Write("Exportation des sites -> CSV, statut : ");

                    using (var memoryStream = new MemoryStream())
                    {
                        string csvFilePathTeam = remoteFilePath + "Cegedim-SITE-" + dateExcel + ".csv";

                        try
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                // Écrire l'en-tête des colonnes
                                writer.WriteLine("SITE;AGENT_ID");

                                foreach (TeamsMembers teamMembers in list_teamsMembers)
                                {
                                    foreach (RequestAPI.TeamMembers member in teamMembers.ListMembersOfTeam)
                                    {
                                        //Ajouter le filtre de Cegedim : 
                                        if (siteFilter.Contains(teamMembers.teamNom) || siteFilter.Count == 0)
                                        {
                                            writer.WriteLine($"{teamMembers.teamNom};{member.agentID}");
                                        }
                                    }
                                }

                                // Flush le contenu du fichier dans le flux mémoire
                                writer.Flush();

                                // Positionner le curseur du flux mémoire au début
                                memoryStream.Position = 0;

                                // Transférer le contenu du flux mémoire vers le serveur SFTP
                                client.UploadFile(memoryStream, csvFilePathTeam);

                                logFile.WriteLine("OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            logFile.WriteLine("KO : " + ex.Message);
                        }

                    }
                   
                }
                else
                {
                    logFile.WriteLine("KO : La connexion SFTP a échoué.");
                }

                // Déconnecter du serveur SFTP
                client.Disconnect();
            }

            //fermer le fichier de log à la fin de l'exécution de l'application
            logFile.Close();

            Console.WriteLine("-------------------------------------------------Stop-------------------------------------------------");

        }
    }
}
