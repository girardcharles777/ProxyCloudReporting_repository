﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyCloudReportingExecutable
{
    internal class ParticipantDataTable
    {
        public string conversationID;
        public List<RequestAPI.ParticipantAttributes> listparticipantAttributes;
        public ParticipantDataTable(string _conversationID, List<RequestAPI.ParticipantAttributes> _listparticipantAttributes) 
        {
            this.conversationID = _conversationID;
            this.listparticipantAttributes = _listparticipantAttributes;
        }
    }
}
